package swagui

import "html/template"

var tmpl = template.Must(template.New("swagui").Parse(`<html>
<head>
    <title>{{.Title}}</title>

    <!-- Swagger UI -->
    <link rel="stylesheet" href="https://unpkg.com/swagger-ui-dist@3/swagger-ui.css"/>
    <script src="https://unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js" charset="UTF-8"></script>
</head>
<body>
<div id="swagger-ui"></div>
</body>

<script>
    {{.Script}}
</script>
</html>`))
