# Swagger UI

A quick router implementation of Swagger UI.

## Configuration

You can pass your own entire JS setup using `swagui.WithScript()`. This seemed simpler than trying to 
re-create all the options in Go.

The default configuration is just how I like to present Swagger UI, so it may need some adjustments for your liking.

See Swagger UI [Configuration](https://swagger.io/docs/open-source-tools/swagger-ui/usage/configuration/).

## Examples

[swagui](_example/swagui/main.go)  
`go run _example/swagui/main.go`

[default from Swagger UI documentation](_example/default/main.go)  
`go run _example/default/main.go`

## License

[MIT](LICENSE)