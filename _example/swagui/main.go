package main

import (
	"fmt"
	"go.jolheiser.com/swagui"
	"net/http"
)

func main() {
	http.Handle("/", swagui.New("https://petstore.swagger.io/v2/swagger.json"))
	fmt.Println("Listening on http://localhost:8000")
	fmt.Println(http.ListenAndServe(":8000", nil))
}
