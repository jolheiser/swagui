package main

import (
	"fmt"
	"go.jolheiser.com/swagui"
	"net/http"
)

var script = `const ui = SwaggerUIBundle({
    url: "${specURL}",
    dom_id: '#swagger-ui',
    presets: [
      SwaggerUIBundle.presets.apis,
      SwaggerUIBundle.SwaggerUIStandalonePreset
    ]
  })`

func main() {
	http.Handle("/", swagui.New("https://petstore.swagger.io/v2/swagger.json", swagui.WithTitle("Default"), swagui.WithScript(script)))
	fmt.Println("Listening on http://localhost:8000")
	fmt.Println(http.ListenAndServe(":8000", nil))
}
